module.exports = api => {
  const itTest = api.env('test');

  return {
    presets: [
      ['@babel/preset-env', { targets: { node: 'current' } }],
      '@babel/preset-typescript'
    ]
  };
}
