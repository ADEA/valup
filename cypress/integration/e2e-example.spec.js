/// <reference types="cypress" />

describe('Example apps', () => {
  beforeEach(() => {
    cy.visit('http://localhost:8888/example/index.html')
  })

  describe('Counter app', () => {
    it('can increment', () => {
      cy.get('#counter-display').should('have.text', 0)
      cy.get('#counter-increment').click()
      cy.get('#counter-increment').click()
      cy.get('#counter-display').should('have.text', 2)
    })
    it('can decrement', () => {
      cy.get('#counter-display').should('have.text', 0)
      cy.get('#counter-decrement').click()
      cy.get('#counter-decrement').click()
      cy.get('#counter-display').should('have.text', -2)
    })
  })

  describe('I18n app', () => {
    it('should update on input', () => {
      cy.get('#i18n-display').should('have.text', 'Hello, Alice!')
      cy.get('#i18n-username').type('A')
      cy.get('#i18n-display').should('have.text', 'Hello, A!')
      cy.get('#i18n-username').type('l')
      cy.get('#i18n-display').should('have.text', 'Hello, Al!')
      cy.get('#i18n-username').type('i')
      cy.get('#i18n-display').should('have.text', 'Hello, Ali!')
      cy.get('#i18n-username').type('c')
      cy.get('#i18n-display').should('have.text', 'Hello, Alic!')
      cy.get('#i18n-username').type('e')
      cy.get('#i18n-display').should('have.text', 'Hello, Alice!')
    })

    it('should update on locale', () => {
      cy.get('#i18n-display').should('have.text', 'Hello, Alice!')
      cy.get('#i18n-locale').select('en')
      cy.get('#i18n-display').should('have.text', 'Hello, Alice!')
      cy.get('#i18n-locale').select('fr')
      cy.get('#i18n-display').should('have.text', 'Bonjour, Alice !')
      cy.get('#i18n-locale').select('en')
      cy.get('#i18n-display').should('have.text', 'Hello, Alice!')
    })
  })
})
