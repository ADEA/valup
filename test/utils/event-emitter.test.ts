import { EventEmitter } from "../../src/utils/event-emitter";

describe("Prototype", () => {
  test("should be constructible", () => {
    try {
      let tmp = new EventEmitter();
      expect(tmp).toBeInstanceOf(EventEmitter);
      expect(typeof tmp).toMatch(/^object$/);
    } catch {
      fail();
    }
  });

  test("should have stringTag", () => {
    const stringified = Object.prototype.toString.call(new EventEmitter());
    expect(stringified).toMatch(/^\[object EventEmitter\]$/);
  });

  test("should have properties on instances", () => {
    const instance = new EventEmitter();
    expect(instance.events).toBeDefined();
    expect(instance.events).toBeInstanceOf(Map);
  });

  test("should own methods", () => {
    expect(EventEmitter?.prototype?.emit).toBeDefined();
    expect(EventEmitter?.prototype?.on).toBeDefined();
    expect(EventEmitter?.prototype?.once).toBeDefined();
    expect(EventEmitter?.prototype?.removeHandler).toBeDefined();
  });
});

describe("Usage", () => {
  test("should work with a simple empty event", () => {
    const loggerMock = jest.fn();
    const instance = new EventEmitter();
    const event = "TEST";

    instance.on(event, loggerMock);
    expect(instance.events.get(event)).toStrictEqual([loggerMock]);
    expect(loggerMock).not.toHaveBeenCalled();
    instance.emit(event);
    instance.emit(event);
    expect(loggerMock).toHaveBeenCalledTimes(2);
  });

  test("should fire the right event", () => {
    const loggerMock1 = jest.fn();
    const loggerMock2 = jest.fn();
    const instance = new EventEmitter();
    const event1 = "TEST1";
    const event2 = "TEST2";

    instance.on(event1, loggerMock1);
    instance.on(event2, loggerMock2);

    instance.emit(event1);
    instance.emit(event1);
    instance.emit(event1);
    instance.emit(event2);
    instance.emit(event2);

    expect(loggerMock1).toHaveBeenCalledTimes(3);
    expect(loggerMock2).toHaveBeenCalledTimes(2);
  });

  test("should use given arguments", () => {
    const loggerMock = jest.fn((...args: any[]) => { });
    const instance = new EventEmitter();
    const event = "TEST";
    const args1 = { state: { test: {}, placeholder: "test" } };
    const args2 = [ 123, "Hello, world!" ];

    instance.on(event, loggerMock);
    instance.emit(event, args1);
    expect(loggerMock).toHaveBeenCalledWith(args1);
    instance.emit(event, args2);
    expect(loggerMock).toHaveBeenCalledWith(args2);
  });

  test("should correctly remove handlers", () => {
    const loggerMock = jest.fn((...args: any[]) => { });
    const instance = new EventEmitter();
    const event = "TEST";

    instance.on(event, loggerMock);
    instance.emit(event);
    expect(loggerMock).toHaveBeenCalledTimes(1);
    instance.removeHandler(event, loggerMock);
    instance.emit(event);
    expect(loggerMock).toHaveBeenCalledTimes(1);
    expect(instance.events.get(event)).toStrictEqual([]);
  });

  test("should be immediately removed when 'once' is used", () => {
    const loggerMock = jest.fn();
    const instance = new EventEmitter();
    const event = "TEST";

    instance.once(event, loggerMock);
    instance.emit(event);
    instance.emit(event);
    instance.emit(event);
    expect(loggerMock).toHaveBeenCalledTimes(1);
    expect(instance.events.get(event)).toStrictEqual([]);
  });
});
