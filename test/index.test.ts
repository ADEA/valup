import { R } from "../src/index";

describe("Prototype", () => {
  test("should be constructible with factory methods", () => {
    expect(R.data(0)).toBeInstanceOf(R);
    expect(R.value(0)).toBeInstanceOf(R);
  });

  test("should have stringTag", () => {
    expect(Object.prototype.toString.call(R.data(0))).toBe("[object R]");
  });

  test("should have methods", () => {
    expect(R?.prototype?.addHandler).toBeDefined();
    expect(R?.prototype?.notify).toBeDefined();
    expect(R?.prototype?.notifyChanged).toBeDefined();
    expect(R?.prototype?.notifyChanging).toBeDefined();
    expect(R?.prototype?.removeHandler).toBeDefined();
    expect(R?.prototype?.on).toBeDefined();
  });
});

describe("Usage", () => {
  test("should allow for get/set", () => {
    const givenValues = ["Hi", 5, null, jest.fn(), { a: 'b' }, []];
    const data = R.data(null as any);
    givenValues.forEach((item, index: number) => {
      data.val = item;
      expect(data.val).toBe(item);
      expect(data.val).toBe(givenValues[index]);
    });
  });

  test("should emit custom events", () => {
    const fnMock = jest.fn();
    const event = "CUSTOM";
    const data = R.data(0).on(event, fnMock);

    expect(fnMock).not.toHaveBeenCalled();
    data.notify(event, data.currentState);
    data.notify(event, data.currentState);
    expect(fnMock).toHaveBeenCalledTimes(2);
  });

  test("should remove handlers correctly", () => {
    const fnMock = jest.fn();
    const event = "CUSTOM";
    const data = R.data(0).on(event, fnMock);

    data.notify(event, data.currentState);
    data.removeHandler(event, fnMock);
    data.notify(event, data.currentState);
    data.notify(event, data.currentState);
    expect(fnMock).toHaveBeenCalledTimes(1);
  });

  test("should notify when non-strict", () => {
    const changingMock = jest.fn();
    const changedMock = jest.fn();
    const data = R.data(0)
      .on("changing", changingMock)
      .on("changed", changedMock);
    data.val = 0;
    data.val = 0;
    expect(changingMock).toHaveBeenCalledTimes(2);
    expect(changedMock).toHaveBeenCalledTimes(2);
  });

  test("should not notify when strict", () => {
    const changingMock = jest.fn();
    const changedMock = jest.fn();
    const data = R.value(0)
      .on("changing", changingMock)
      .on("changed", changedMock);
    data.val = 0;
    data.val = 0;
    expect(changingMock).toHaveBeenCalledTimes(0);
    expect(changedMock).toHaveBeenCalledTimes(0);
  });

  test("should notify on strict only when equality function applies", () => {
    type Identity = {id: string, name?: string};
    const eqCheck1 = jest.fn((a: Identity, b: Identity) => a.id === b.id);
    const eqCheck2 = jest.fn((a: Array<unknown>, b: Array<unknown>) => a.length === b.length);
    const changedMock = jest.fn();

    const data1 = R.value({id: "first"} as Identity, eqCheck1).on("changed", changedMock);
    const data2 = R.value([1, 2], eqCheck2).on("changed", changedMock);

    data1.val = {id: "first", name: "hi"};
    expect(changedMock).not.toHaveBeenCalled();
    data1.val = {id: "last", name: "hi"};
    expect(changedMock).toHaveBeenCalledTimes(1);
    data2.val = ["Hello", "World"];
    expect(changedMock).toHaveBeenCalledTimes(1);
    data2.val = [1,,2];
    expect(changedMock).toHaveBeenCalledTimes(2);
  });

  test("should allow recursive", () => {
    let counter = 0;
    const counterFn = jest.fn(() => { counter++ });
    const instance = R.data(0).on("changing", () => {
      if (counter < 10) {
        counterFn();
        instance.notify("changing", instance.currentState);
      }
    });

    expect(counterFn).not.toHaveBeenCalled();
    instance.val = 0;
    expect(counterFn).toHaveBeenCalledTimes(10);
    expect(counter).toBe(10);
  });

  test("should work with chained reactive signals", () => {
    const [fn1, fn2, fn3] = [jest.fn(), jest.fn(), jest.fn()];
    const [i1, i2, i3] = [R.data(0), R.data(0), R.data(0)];
    const [e1, e2, e3] = ["ONE", "TWO", "THREE"];
    i1.on(e1, () => {fn1(); i2.notify(e2, i1.currentState)});
    i2.on(e2, () => {fn2(); i3.notify(e3, i2.currentState)});
    i3.on(e3, fn3);

    i1.notify(e1, i1.currentState);
    i2.notify(e2, i2.currentState);
    i3.notify(e3, i3.currentState);

    expect(fn1).toHaveBeenCalledTimes(1);
    expect(fn2).toHaveBeenCalledTimes(2);
    expect(fn3).toHaveBeenCalledTimes(3);
  });

  test("should execute handler when any watched reactive value updates", () => {
    const [fn1, fn2, fn3, fn4] = Array.from({length: 4}, () => jest.fn());
    const [i1, i2, i3] = [R.data(0), R.data(0), R.data(0)];

    R.onAnyChanging([i1, i2, i3], fn1);
    R.onAnyChanged([i1, i2, i3], fn2);
    R.onAny([i1, i2, i3], "changing", fn3);
    R.onAny([i1, i2, i3], "changed", fn4);
    i2.val = 0;
    i1.notifyChanged({});
    i3.notifyChanging({});

    expect(fn1).toHaveBeenCalledTimes(2);
    expect(fn2).toHaveBeenCalledTimes(2);
    expect(fn3).toHaveBeenCalledTimes(2);
    expect(fn4).toHaveBeenCalledTimes(2);
  });
});
