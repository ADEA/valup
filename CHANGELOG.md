# CHANGELOG

## 0.2.0

- Fix npm module for import/require
- Static listeners for multiple reactive values (`onAny`, `onAnyChanged`, `onAnyChanging`)
- Renamed 'listeners' to 'handlers' everywhere, because that's what they really are.
- Documentation

## 0.1.0

First version.
