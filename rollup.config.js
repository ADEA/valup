export default {
  input: "build/index.js",
  output: [
    {
      file: "dist/mjs/valup.js",
      format: "es",
    },
    {
      file: "dist/cjs/valup.js",
      format: "cjs",
    },
    {
      file: "dist/umd/valup.js",
      format: "umd",
      name: "valup",
    },
  ],
};
