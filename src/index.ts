import { EventEmitter } from './utils/event-emitter'

/**
 * Optional properties for the {@link R.constructor}.
 * @typeParam T - The value's type.
 * @typedef
 */
interface RProps<T> {
  /**
   * The initial valie of the reactive object.
   * @defaultValue undefined
   */
  value?: T;

  /**
   * If true, the "changing"/"changed" events will only be fired
   * when the next value is different from the last.
   * @defaultValue false
   */
  strictValue?: boolean;
}

/**
 * Possible state object passed to an event handler.
 * @typeParam T - The type of the referenced `R` object's value.
 * @typedef
 */
interface _RState<T> {
  /**
   * Previous value, before change.
   */
  prev?: any,
  /**
   * Current value at all times.
   */
  current?: any,
  /**
   * Next value, after change.
   */
  next?: any,
  /**
   * Reference to the `R` instance that fired the event.
   */
  target: R<T>,
  /**
   * Any special, user defined, property.
   */
  prop?: any
}

/**
 * Reactive state used in "changing"/"changed" event handlers.
 * @typedef
 */
type RState = _RState<any> | any;

/**
 * The reactive value wrapper. This is the class you want to use.
 * @typeParam T - The value's type, never forced in the code, helps when writing
 *  typescript and wanting a specific type.
 */
export class R<T> {
  /**
   * Internal value.
   */
  #val: any = undefined;

  /**
   * EventEmitter internal instance.
   * Used to map events/handlers.
   */
  #eventEmitter: EventEmitter = new EventEmitter();

  /**
   * If true, the "changing"/"changed" events will only be fired
   * when the next value is different from the last.
   */
  #strictValue: boolean = false;

  /**
   * Equality function check provided when comparing values on "strictValue".
   * @param a - Left argument
   * @param b - Right argument
   * @returns True if the arguments are "equal" (depending on implementation), false otherwise.
   */
  public equalityCheck: Function = (a: any, b: any) => a == b;

  /**
   * Changes the string tag.
   * @example
   * ```js
   * console.log(R.value(0).toString());
   * // "[object R]"
   * ```
   */
  public get [Symbol.toStringTag]() {
    return 'R';
  }

  /**
   * Gets the current object state: its current value and a reference to itself.
   */
  public get currentState(): RState {
    return { current: this.#val, target: this };
  }

  /**
   * Reactive variable getter.
   */
  public get val() {
    return this.#val;
  }
  /**
   * Reactive variable setter.
   * Fires "changing" and "changed" events.
   */
  public set val(value: any) {
    const state: RState = { prev: this.#val, current: this.#val, next: value, target: this };
    const shouldNotify = this.#strictValue && !this.equalityCheck(state.prev, state.next) || !this.#strictValue;
    if (shouldNotify) {
      this.notifyChanging(state);
    }
    this.#val = value;
    state.current = this.#val;
    if (shouldNotify) {
      this.notifyChanged(state);
    }
  }

  /**
   * Constructs a reactive variable.
   * @param param0 - The object containing it's initial value and "strict" mode option.
   */
  private constructor({ value = undefined, strictValue = false }: RProps<T>) {
    this.#val = value;
    this.#strictValue = strictValue;
  }

  /**
   * Factory method for a **non-strict** reactive variable.
   * @param value - Initial value.
   * @returns Instance of a reactive variable.
   */
  public static data<T>(value: T) {
    return new R<T>({ value, strictValue: false });
  }

  /**
   * Factory method for a **strict** reactive variable.
   * @param value - Initial value.
   * @returns Instance of a reactive variable.
   */
  public static value<T>(value: T, equalityCheck?: Function) {
    const r = new R<T>({ value, strictValue: true });
    r.equalityCheck = equalityCheck ?? ((a: any, b: any) => a == b);
    return r;
  }

  /**
   * Adds an event handler.
   * @param event - The event to listen to.
   * @param handler - The handler function to apply.
   * @returns Itself, for chaining purposes.
   */
  public addHandler(event: string, handler: Function) {
    this.#eventEmitter.on(event, handler);
    return this;
  }

  /**
   * Adds an event handler. (alias of {@link addHandler})
   * @param event - The event to listen to.
   * @param handler - The handler function to apply.
   * @returns Itself, for chaining purposes.
   */
  public on(event: string, handler: Function) {
    return this.addHandler(event, handler);
  };

  /**
   * Removes an event handler previously added.
   * @param event - The event to listen to.
   * @param handler - The handler function to remove from the map.
   * @returns Itself, for chaining purposes.
   */
  public removeHandler(event: string, handler: Function) {
    this.#eventEmitter.removeHandler(event, handler);
    return this;
  }

  /**
   * Emits a "changing" event.
   * @see {@link notify}
   * @param state - User provided state for the handlers.
   * @returns Itself, for chaining purposes.
   */
  public notifyChanging(state: RState) {
    return this.notify("changing", state);
  }

  /**
   * Emits a "changed" event.
   * @see {@link notify}
   * @param state - User provided state for the handlers.
   * @returns Itself, for chaining purposes.
   */
  public notifyChanged(state: RState) {
    return this.notify("changed", state);
  }

  /**
   * Emits an event.
   * @param event - The event to emit.
   * @param state - User provided state for the handlers.
   * @returns Itself, for chaining purposes.
   */
  public notify(event: string, state: RState) {
    this.#eventEmitter.emit(event, { state });
    return this;
  }

  /**
   * Adds a common "changing" handler to multiple reactive variables.
   * @see {@link R.onAny}
   * @param toListen - The list of variables to listen to.
   * @param handler - The handler to apply.
   */
  public static onAnyChanging(toListen: Array<R<any>>, handler: Function) {
    R.onAny(toListen, "changing", handler)
  }

  /**
   * Adds a common "changed" handler to multiple reactive variables.
   * @see {@link R.onAny}
   * @param toListen - The list of variables to listen to.
   * @param handler - The handler to apply.
   */
  public static onAnyChanged(toListen: Array<R<any>>, handler: Function) {
    R.onAny(toListen, "changed", handler)
  }

  /**
   * Adds a common event handler to multiple reactive variables.
   * @see {@link addHandler}
   * @param toListen - The list of variables to listen to.
   * @param event - The event to listen to.
   * @param handler - The handler to apply.
   */
  public static onAny(toListen: Array<R<any>>, event: string, handler: Function) {
    toListen.forEach((reactive: R<any>) => {
      reactive.on(event, handler)
    });
  }
}
