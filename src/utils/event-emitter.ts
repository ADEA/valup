
/**
 * Event handler used in {@link EventEmitter}
 */
type EventHandlerFunction = Function;

/**
 * The "event:handlers" internal map in {@link EventEmitter}
 */
type EventMap = Map<string, Array<EventHandlerFunction>>;

/**
 * Event Emitter/Listener object.
 * Used in the `R` class.
 */
export class EventEmitter {
  /**
   * "event:handlers" map.
   */
  #events: EventMap = new Map();

  /**
   * Getter for events map, makes sure it stays a map.
   * @readonly
   */
  public get events() {
    if (!(this.#events instanceof Map)) {
      this.#events = new Map();
    }
    return this.#events;
  }

  /**
   * Changes the string tag.
   * @example
   * ```js
   * console.log((new EventEmitter()).toString());
   * // "[object EventEmitter]"
   * ```
   */
  public get [Symbol.toStringTag]() {
    return 'EventEmitter';
  }

  /**
   * Adds an event handler.
   * @param event - The event to listen to.
   * @param handler - The handler function to apply.
   */
  public on(event: string, handler: EventHandlerFunction) {
    let registered = this.events.get(event);
    if (typeof registered !== "object") {
      registered = [];
    }
    registered.push(handler);
    this.events.set(event, registered);
  }

  /**
   * Removes an event handler.
   * @param event - The event to listen to.
   * @param handler - The handler function to remove from the map.
   */
  public removeHandler(event: string, handler: EventHandlerFunction) {
    const index = this.events.get(event)?.indexOf(handler) ?? -1;
    if (index <= -1) return;

    const registered = this.events.get(event);
    registered!.splice(index, 1);
    this.events.set(event, registered!);
  }

  /**
   * Fires an event and execute all of its handlers.
   * @param event - The event to emit.
   * @param args - The optional arguments to pass onto the handlers.
   */
  public emit(event: string, ...args: any) {
    const registered = this.events.get(event);
    if (typeof registered !== "object") return;

    const applyArgs = (f: EventHandlerFunction) => { f.apply(this, args) };
    registered!.forEach(applyArgs);
  }

  /**
   * Creates an event handler that is instantly removed when called.
   * Hence the name 'once' since it can be fired only once.
   * @param event - The event to listen to.
   * @param handler - The handler function to apply.
   */
  public once(event: string, handler: EventHandlerFunction) {
    const self = this;
    this.on(event, function callback(...args: any) {
      self.removeHandler(event, callback);
      handler.apply(self, args);
    });
  }
}
