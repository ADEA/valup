/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  collectCoverage: true,
  coverageReporters: ["clover", "json", "lcov", "text", "cobertura"],
  testMatch: ["**/test/**/*.test.ts"]
};
